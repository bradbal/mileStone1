#ifndef nodeMake_H
#define nodeMake_H

struct node {

    int nodeNumber;
    int totalNodes;
    int nNextNodes;
    int dist;
    struct node** nextNodes;

};

struct centreNodes {
   int* metricOne;
   int* metricTwo;
   int nMetricOne;
   int nMetricTwo;
};

#endif

void freeNodesPointer(struct node* nodes);
void freeNodesPointerPointer(struct node** nodes);
struct node* read_nodeFile(FILE* file);
void freeCentreNodesPointer(struct centreNodes* centres);
