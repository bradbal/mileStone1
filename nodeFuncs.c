#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <nodeFuncs.h>

struct node;
struct centreNodes;

void freeNodesPointer(struct node* nodes){
        int nNodes = nodes[0].totalNodes;
        for(int n=0; n<nNodes; n++){

                free( (nodes[n]).nextNodes );
        }

        free(nodes);
}

void freeNodesPointerPointer(struct node** nodes){
        int nNodes = (**nodes).totalNodes;
        for(int n=0; n<nNodes; n++){

                free( (*nodes[n]).nextNodes );
        }

        free(nodes);
}

/*function to read node files*/
struct node* read_nodeFile(FILE* file){
        int next;
        char nextChar;
        int nNodes;
        fscanf(file, "%d", &nNodes);
        struct node* nodes = (struct node*)malloc(sizeof(struct node)*nNodes);
        int position = 0;

        //creating nNodes new nodes and storing in nodes
        for (int n=0; n<nNodes; n++){
                struct node nodex;
                nodex.nodeNumber = n;
                nodex.totalNodes = nNodes;
                nodes[n] = nodex;
        }

        while(position<nNodes){

                struct node* currentNode = &nodes[position];
                struct node** nextNodes = (struct node**)malloc( sizeof(struct node)*(nNodes-1) );

                //reading nodes attached to
                //fscanf(file, "%d%c", &next, &nextChar);
                int i = 0;
                //while(nextChar != '\n'){
                do {
                        fscanf(file, "%d%c", &next, &nextChar);
                        nextNodes[i] = &nodes[ next ];
                        i++;
                } while (nextChar != '\n');

                currentNode->nextNodes = nextNodes;
                currentNode->nNextNodes = i;
                position++;
        }
        fclose(file);

	//making graph undirected if not implicit in node file
	for (int i=0; i<nNodes; i++){
	    struct node* nodei = &nodes[i];
	    int nNextNodes = nodei->nNextNodes;
	    struct node** nextNodes = nodei->nextNodes;

	    //checking each child of nodei has nodei as a child
	    for (int j=0; j<nNextNodes; j++){
		struct node* nodej = nextNodes[j];
		struct node** childsChildren = nodej->nextNodes;
		int childsNNextNodes = nodej->nNextNodes;
		int isDirected = 0; //assuming isn't directed to start

		//checking children by reference
		for (int k=0; k<childsNNextNodes; k++){
		    if (childsChildren[k]==nodei){
			isDirected = 1;
		    }
		}

		//if child of nodei dosn't have nodei as child, put it as child
		if (!isDirected){
		    childsChildren[childsNNextNodes] = nodei;
		    nodej->nNextNodes = childsNNextNodes + 1;
		}
	    }
	}

        return nodes;
}

void freeCentreNodesPointer(struct centreNodes* centres){
        free(centres->metricOne);
        free(centres->metricTwo);
        free(centres);
}
