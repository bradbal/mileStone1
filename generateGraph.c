#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void writeNodeFile(FILE* nodefile, int N, int totalNodes, int bFactor){

	//total nodes go at top of file
	fprintf(nodefile, "%d\n", totalNodes);

	//given node only connects to one other node
	if (bFactor == 1) {
	   //adding first node connecting to next node
	   fprintf(nodefile, "%d\n", 1);
	   for (int i=1; i<totalNodes-1; i++){
		//connects to next node and previous node
		fprintf(nodefile, "%d %d\n", i-1, i+1);
	   }
	   //last node only connects to previous node
	   fprintf(nodefile, "%d\n", totalNodes-2);

	//connects to two other nodes
	} else if (bFactor == 2) {
	   //adding first node connect to next node and node 6 ahead
	   fprintf(nodefile, "%d %d\n", 1, 6);

	   for (int i=1; i<5; i++){
                //connects to next node & previous node & node 6 ahead
                fprintf(nodefile, "%d %d %d\n", i-1, i+1, i+6);
           }

	   //due to structure of graph
	   fprintf(nodefile, "%d %d\n", 4, 16);

	   for (int i=6; i<totalNodes-7; i++){
		//due to structure of graph
		if ((i-5)%11>0){
		   //connects to next node & previous node & node 6 ahead
                   fprintf(nodefile, "%d %d %d\n", i-1, i+1, i+6);
		} else{
		   fprintf(nodefile, "%d %d\n", i-1, i+11);
		}
	   }

	   //since can't connect to next 6 nodes that aren't there
	   for (int i=totalNodes-7; i<totalNodes-1; i++){
		//connects to next node & previous node
		fprintf(nodefile, "%d %d\n", i-1, i+1);
	   }

           //last node only connects to previous node
           fprintf(nodefile, "%d\n", totalNodes-2);

	//average node connects to 3 other nodes
	} else{
	   //adding first node connect to next node, node 6 ahead & node 11 ahead
           fprintf(nodefile, "%d %d %d\n", 1, 6, 11);

           for (int i=1; i<5; i++){
                //connects to next node & previous node & node 6 ahead & 11 ahead
                fprintf(nodefile, "%d %d %d %d\n", i-1, i+1, i+6, i+11);
           }

           //due to structure of graph
           fprintf(nodefile, "%d %d\n", 4, 16);

           for (int i=6; i<totalNodes-11; i++){
	      //due to structure of graph
                if ((i-5)%11>0){
                   //connects to next node & previous node & node 6 ahead
                   fprintf(nodefile, "%d %d %d %d\n", i-1, i+1, i+6, i+11);
                } else{
                   fprintf(nodefile, "%d %d\n", i-1, i+11);
                }
           }


	   //since can't connect to next 11 nodes that aren't there
           for (int i=totalNodes-11; i<totalNodes-7; i++){
		//connects to next node & previous node & node 6 ahead
                fprintf(nodefile, "%d %d %d\n", i-1, i+1, i+6);
           }

           //since can't connect to next 6 nodes that aren't there
           for (int i=totalNodes-7; i<totalNodes-1; i++){
                //connects to next node & previous node
                fprintf(nodefile, "%d %d\n", i-1, i+1);
           }

           //last node only connects to previous node
           fprintf(nodefile, "%d\n", totalNodes-2);

	}

	fflush(nodefile);
	fclose(nodefile);
}

void writeCoordFile(FILE* coordfile, int totalNodes){

    int y=0;
    int x=0;
    //writing up nodes in order in recurring pattern
    for (int i=0; i<totalNodes; i++){
	fprintf(coordfile, "%d %d\n", x, y);
	if(x!=9 && x!=10){
	   x+=2;
	} else if(x==10){
	   x=1;
	   y++;
	} else if(x==9){
	   x=0;
	   y++;
	}
    }

   fflush(coordfile);
   fclose(coordfile);
}

int main(int argc, char** argv) {
	//N factor, related to no. of nodes by n= 11N+16;
	int N;
	//number of nodes
	int totalNodes;
	//branching factor
	int bFactor;
	//name of nodefile to write to
	char* nodefileName;
	//name of coord fine to write to
	char* coordfileName;

	if (argc == 5) {
	   //reading in inputs
	   sscanf(argv[1], "%i", &N);
	   totalNodes = 11*N + 17;
	   sscanf(argv[2], "%i", &bFactor);
	   nodefileName = argv[3];
	   coordfileName = argv[4];

	} else{
	   printf("Not enough input arguments");
	   return 1;
	}

	//writing node file
	FILE* nodefile = fopen(nodefileName, "w");
	writeNodeFile(nodefile, N, totalNodes, bFactor);

	//writing the coord file
	FILE* coordfile = fopen(coordfileName, "w");
	writeCoordFile(coordfile, totalNodes);
	return 0;
}
