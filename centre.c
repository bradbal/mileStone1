#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <nodeFuncs.h>

struct node;
struct centreNodes;

void getDistances(int queueSize, struct node** queue, int nDistances, int* distances, int nNodesChecked,
                  struct node** nodesChecked);

int* BreadthFirstSearch(struct node* currentNode){

	//initialising variables for recursion
	struct node** nodesChecked=malloc(sizeof(struct node*)* \
				        (currentNode->totalNodes));
	int* distances = malloc(sizeof(int)*((currentNode->totalNodes)-1));
	nodesChecked[0] = currentNode;
	currentNode->dist = 0;
	int nNodesChecked = 0;

	struct node** queue = malloc(sizeof(struct node*)* \
				     (currentNode->totalNodes));
	queue[0] = currentNode;

	getDistances(1, queue, 0, distances, 1, nodesChecked);

	//freeing our malloc'd space
	free(nodesChecked);
	free(queue);
	return distances;
}

void getDistances(int queueSize, struct node** queue, int nDistances, int* distances, int nNodesChecked,
		  struct node** nodesChecked){

	if (queueSize == 0){
	   return ;
	}

	struct node* currentNode = queue[0]; 
	int nChildren = currentNode->nNextNodes;

	struct node** children = currentNode->nextNodes;
	int parentDist = currentNode->dist;

	//adding distances for every child of current node if not already checked
	for (int i=0; i<nChildren; i++){
	   struct node* childi = children[i];

	   //checking child hasn't already been checked
	   int childNew = 1;
	   for (int j=0; j<nNodesChecked; j++) {
	      struct node* nodesCheckedj = nodesChecked[j];
	      if (childi == nodesCheckedj){
		childNew = 0;
		break;
	      }
	   }

	   if (childNew){

		//updating the distances
		nDistances = nDistances + 1;
		nNodesChecked = nNodesChecked + 1;
		childi->dist = parentDist + 1;
		distances[nDistances-1] = parentDist + 1;
		nodesChecked[nNodesChecked-1] = childi;

		//returning distances if we have distances to every other node
		if (nDistances == (childi[0].totalNodes) ){
		    return ;
		}

		//adding child to queue
		queueSize++;
		queue[queueSize-1] = childi;

	   }
	}

	//removing first element from queue
	for (int i=1; i<queueSize; i++){
		struct node* temp = queue[i];
		queue[i-1] = temp;
	}
	queueSize = queueSize - 1;
	getDistances(queueSize, queue, nDistances, distances, nNodesChecked, nodesChecked);
} 

//Where distances[n] is node ns distances returned from above method
struct centreNodes* getCentres(int** distances, int totalNodes){

	//for storing sums of distances, sums[n] sum for node[n]
	int* sums = malloc(sizeof(int)*(totalNodes));

	//for storing all of the max distances, max[n] max distance for node n.
	int* max = malloc(sizeof(int)*(totalNodes));

	/**getting max distances for each node & sums for each node**/
	for (int i=0; i<totalNodes; i++){

	   int* nodeiDistances = distances[i];
	   sums[i] = 0;
	   max[i] = 0;
	   for (int j=0; j<totalNodes-1; j++){
		//getting sum
	       sums[i] = sums[i] + nodeiDistances[j];

		//getting max
	       if (nodeiDistances[j]>max[i]){
		   max[i] = nodeiDistances[j];
		}
	   }
	}

	/***getting minimum sum & min max***/
	int minSum = sums[0];
	int minMax = max[0];
	for (int i=1; i<totalNodes-1; i++){

	   //getting min sum
	   if (sums[i]<minSum){
		minSum = sums[i];
	   }

	   //getting min max
	   if (max[i]<minMax){
		minMax = max[i];
	   }
	}

	int* metricOneNodes = malloc(sizeof(int)*totalNodes);
	int* metricTwoNodes = malloc(sizeof(int)*totalNodes);
        int nMetricOne = 0;
	int nMetricTwo = 0;
	/***getting nodes with min sum && min max****/
	for (int i=0; i<totalNodes; i++){

	   //getting nodes with min sum
	   if (sums[i]==minSum){
	      metricOneNodes[nMetricOne] = i;
	      nMetricOne++;
	   }

	   //getting nodes with min max
	   if (max[i]==minMax){
	     metricTwoNodes[nMetricTwo] = i;
	     nMetricTwo++;
	   }
	}

	/*******Storing results in struct*******/
	struct centreNodes* centres = malloc(sizeof(struct centreNodes));
	centres->metricOne = metricOneNodes;
	centres->metricTwo = metricTwoNodes;
	centres->nMetricOne = nMetricOne;
	centres->nMetricTwo = nMetricTwo;

	free(sums);
	free(max);
	return centres;
}

void writeCentresToFile(struct centreNodes* centres){

	//output file to write to
	FILE* outputFile;
	outputFile = fopen("centreOutput.txt", "w");

	/****Writing Metric One Centres****/
	int* metricOneNodes = centres->metricOne;
	int nMetricOne = centres->nMetricOne;

	fprintf(outputFile, "%d\n", nMetricOne);
	//writing up centre line
	for (int i=0; i<nMetricOne-1; i++){
	   fprintf(outputFile, "%d ", metricOneNodes[i]);
	}
	fprintf(outputFile, "%d\n", metricOneNodes[nMetricOne-1]);

	/****Writing Metric Two Centres****/
	int* metricTwoNodes = centres->metricTwo;
	int nMetricTwo = centres->nMetricTwo;

	fprintf(outputFile, "%d\n", nMetricTwo);
	//writing centre line
	for (int j=0; j<nMetricTwo-1; j++){
	   fprintf(outputFile, "%d ", metricTwoNodes[j]);
	}
	fprintf(outputFile, "%d\n", metricTwoNodes[nMetricTwo-1]);

	fflush(outputFile);
	fclose(outputFile);
}

/* returns distances, such that distances[n] gives a pointer to
an array of integers containing the distances of node n from every
other node in the graph */
int** getAllDistances(struct node* input){

	int totalNodes = input[0].totalNodes;
	int** distances = malloc(sizeof(int*)*totalNodes);

	//getting distances to every other node for node n
	for (int n=0; n<totalNodes; n++){
	   distances[n] = BreadthFirstSearch( &input[n] );
	}

	return distances;
}

//frees stored distances
void freeAllDistances(int** distances, int totalNodes){
	for (int i=0; i<totalNodes; i++){
	   free(distances[i]);
	}

	free(distances);
}

int main(int argc, char** argv) {
	FILE* inputFile;
	struct node* input;

	//checking inputted arguments correct
	if (argc > 1){
	    inputFile = fopen(argv[1], "r");

	    if (inputFile == NULL){
		printf("Inputted File not found.\n");
		return 1;
	    }

	} else if (argc == 1){
	    printf("Need node file name as input.\n");
	    return 0;
	}

	//inputted nodes as struct node*
	input = read_nodeFile(inputFile);

	//distances to every other node in graph for each node
	int** distances = getAllDistances(input);

	//getting the centre nodes for both metric1 & metric2
	struct centreNodes* centres = getCentres(distances, input[0].totalNodes);

	//writing the centres to the output file
	writeCentresToFile(centres);

	//free all malloc'd memory still on heap
	freeAllDistances(distances, input[0].totalNodes);
	freeCentreNodesPointer(centres);
	freeNodesPointer(input);

	return 0;
}
