all: centre genvis genGraph

centre: centre.c nodeFuncs.c nodeFuncs.h
	gcc -o centre centre.c nodeFuncs.c -std=c99 -I.

genvis: genvis.cc nodeFuncs.c nodeFuncs.h
	g++ -o genvis genvis.cc nodeFuncs.c -lpthread -lX11 -lm -I.

genGraph: generateGraph.c
	gcc -o genGraph generateGraph.c -std=c99

