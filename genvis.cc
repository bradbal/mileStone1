#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <nodeFuncs.h>
#include <math.h>
#define PI 3.14159265358979323846
#include "CImg.h"
using namespace cimg_library;

struct node;
struct centreNodes;

struct centreNodes* read_centreOutput(FILE* file){
   struct centreNodes* centres = (struct centreNodes*)malloc( sizeof(struct centreNodes) );

   /*********Reading centres node from metric one********/
   //getting no. of centres with metric one
   int nMetricOne;
   fscanf(file, "%d", &nMetricOne);

   //get centre nodes with metric one
   int* metricOneNodes = (int*)malloc( sizeof(int)*nMetricOne );

   char nextChar;
   int i=0;
   do {
      fscanf(file, "%d%c", &metricOneNodes[i], &nextChar);
      i++;
   } while (nextChar != '\n');

   /******Reading centre nodes from metric two******/
   //getting no. of centres with metric two
   int nMetricTwo;
   fscanf(file, "%d", &nMetricTwo);

   //get centre nodes of metric two
   int* metricTwoNodes = (int*)malloc( sizeof(int)*nMetricTwo );

   i=0;
   do {
      fscanf(file, "%d%c", &metricTwoNodes[i], &nextChar);
      i++;
   } while (nextChar != '\n');

   /****Storing results in centreNodes struct*****/
   centres->metricOne = metricOneNodes;
   centres->metricTwo = metricTwoNodes;
   centres->nMetricOne = nMetricOne;
   centres->nMetricTwo = nMetricTwo;

   fclose(file);
   return centres;
}

int** read_CoordsFile(FILE* file, int totalNodes){
   int** coords = (int**)malloc( sizeof(int*)*totalNodes );

   char space;
   char enter;
   /**reading coords for each line in file**/
   for (int i=0; i<totalNodes; i++){
      //allocating space
      int* coordsi = (int*)malloc( sizeof(int)*2 );
      int coordxi;
      int coordyi;

      //reading the coords for current line
      fscanf(file, "%d%c%d%c", &coordxi, &space, &coordyi, &enter);

      //saving coordinates
      coordsi[0] = coordxi;
      coordsi[1] = coordyi;
      coords[i] = coordsi;
   }

   return coords;
}

void freeCoords(int** coords, int totalNodes){
   for (int i=0; i<totalNodes; i++){
      free(coords[i]);
   }

   free(coords);
}

void scaleCoords(int** coords, int windowX, int windowY, int totalNodes){

   //getting max & min for x & y coords
   int maxX=coords[0][0];
   int maxY=coords[0][1];
   int minX=coords[0][0];
   int minY=coords[0][1];
   int xi;
   int yi;
   for (int i=0; i<totalNodes; i++){
      //for X
      xi = coords[i][0];
      //Max
      if (xi>maxX){
         maxX = xi;
      }
      //Min
      if (xi<minX){
         minX = xi;
      }

      //for Y
      yi = coords[i][1];
      //Max
      if (yi>maxY) {
         maxY = yi;
      }
      //Min
      if (yi<minY){
         minY = yi;
      }
   }

   //scaling coords according to window size
   float spacer = 1.5; //determines space between window boundary and graph
   int xTranslation = (int)( (windowX - ( windowX/spacer ) * ( (maxX-minX)/(float)maxX ))/spacer );
   int yTranslation = (int)( (windowY - ( windowY/spacer ) * ( (maxY-minY)/(float)maxY ))/spacer );
   for (int j=0; j<totalNodes; j++){
      //scaling X
      xi = coords[j][0];
      coords[j][0] = (int)( (xi/(float)maxX)*(windowX/spacer) ) + xTranslation;

     //scaling y
     yi = coords[j][1];
     coords[j][1] = (int)( (yi/(float)maxY)*(windowY/spacer) ) + yTranslation;
   }
}

void drawGraph(struct node* input, struct centreNodes* centres, int** coords, int totalNodes){
  int windowX = 800;
  int windowY = 600;

  //scaling inputted coords to window size
  scaleCoords(coords, windowX, windowY, totalNodes);

  //nodeSize = ( ( areaOfWindow/100 ) -> convert this area to radius)
  int nodeSize = sqrt( ( ((windowX*windowY)/(float)(totalNodes*15))/PI ) );

  //size of node label
  int labelSize;
  if (nodeSize>23){
      labelSize = 23;
  } else{
      labelSize = 13;
  }
  //creating image
  CImg<unsigned char> img(windowX, windowY, 1, 3); //Define a 640*400 color image with 8 bits per color comp
  img.fill(0); //set pixels intesity to 0, i.e. black.

  //colour defs
  unsigned char purple[] = { 255, 153, 204 };
  unsigned char orange[] = { 204, 102, 0 };
  unsigned char green[] = { 102, 204, 0 };
  unsigned char mixed[] = { 255, 255, 0 };
  unsigned char black[] = { 0, 0, 0 };
  unsigned char white[] = { 255, 255, 255 };

  //for storing node number as a string
  char nodeN[10];

  //calculating points on surface of unit nodes for edge drawing
  float xtranslation[8];
  float ytranslation[8];
  float angle;
  for (int i=0; i<8; i++){
     angle = (float)(i*PI)/4;
     xtranslation[i] = nodeSize * cos( angle );
     ytranslation[i] = nodeSize * sin( angle );
  }

  /*****drawing nodes******/
  for (int i=0; i<totalNodes; i++){

     //checking if node is metric one centre node
     int isMetricOneCentre = 0;
     int nMetricOne = centres->nMetricOne;
     int* metricOneCentres = centres->metricOne;
     for (int j=0; j<nMetricOne; j++){
        if ( i == metricOneCentres[j] ){
           isMetricOneCentre = 1;
        }
     }

        //checking if node is metric two centre
     int isMetricTwoCentre = 0;
     int nMetricTwo = centres->nMetricTwo;
     int* metricTwoCentres = centres->metricTwo;
     for (int k=0; k<nMetricTwo; k++){
        if ( i == metricTwoCentres[k] ){
           isMetricTwoCentre = 1;
        }
     }

     //drawing nodes appropriate colors if centres or not
     int xi = coords[i][0];
     int yi = coords[i][1];
     if (isMetricOneCentre && isMetricTwoCentre){

        img.draw_circle(xi, yi, nodeSize, mixed); //draw mixed node
     } else if (isMetricOneCentre && isMetricTwoCentre==0){

        img.draw_circle(xi, yi, nodeSize, green); //draw green node
     } else if (isMetricOneCentre==0 && isMetricTwoCentre){

        img.draw_circle(xi, yi, nodeSize, purple); //draw purple node
     } else{

        img.draw_circle(xi, yi, nodeSize, orange); //draw orange node
     }

     //labelling node
     sprintf(nodeN, "%d", i);
     img.draw_text(xi, yi, (const char*)nodeN, black, 0, 1, labelSize);

    /*********Drawing edges*************/
    struct node** nextNodes = input[i].nextNodes;
    int nNextNodes = input[i].nNextNodes;
    int nodek, xk, yk, iTrans, kTrans;
    for (int k=0; k<nNextNodes; k++){
        nodek = nextNodes[k]->nodeNumber;
	xk = coords[nodek][0];
	yk = coords[nodek][1];

	//if i node left of k node
	if (xi<xk && yi==yk){
		iTrans = 0;
	//if i node bottom left to k node
	} else if (xi<xk && yi>yk) {
		iTrans = 7;
	//if i node directly below k node
        } else if (xi==xk && yi>yk) {
		iTrans = 6;
	//if i bottom right of k
	} else if (xi>xk && yi>yk) {
		iTrans = 5;
	//if i right of k
	} else if (xi>xk && yi==yk) {
		iTrans = 4;
	//if i top right of k
	} else if (xi>xk && yi<yk) {
		iTrans = 3;
	//if i above k
	} else if (xi==xk && yi<yk) {
		iTrans = 2;
	//if i top left to k
	} else if (xi<xk && yi<yk) {
		iTrans = 1;
	}

	//if i below k by some angle
	if (iTrans <= 3){
		kTrans = iTrans+4;
	//if i above k by some angle
	} else {
		kTrans = iTrans-4;
	}

	//now drawing the edges from points of surface nodei & nodek
	img.draw_line(xi+xtranslation[iTrans], yi+ytranslation[iTrans], xk+xtranslation[kTrans],\
		      yk+ytranslation[kTrans], white);
    }
  }

  /****Draw Legend*******/
  float legendNodeSize = nodeSize-(nodeSize/(float)2);
  if (legendNodeSize>20){
      legendNodeSize=20;
  }
  int loc1, loc2, loc3, loc4;
  //non-central nodes
  loc1 = 20;
  img.draw_circle(loc1, loc1, legendNodeSize, orange);
  img.draw_text(loc1+legendNodeSize, loc1, "Non-central node", white, 0, 1, 15);
  //central nodes metric one
  loc2 = loc1+legendNodeSize*2+5;
  img.draw_circle(loc1, loc2, legendNodeSize, green);
  img.draw_text(loc1+legendNodeSize, loc2, "Metric one centre", white, 0, 1, 15);
  //central nodes metric two
  loc3 = loc1+legendNodeSize+160;
  loc4 = loc3+legendNodeSize;
  img.draw_circle(loc3, loc1, legendNodeSize, purple);
  img.draw_text(loc4, loc1, "Metric two centre", white, 0, 1, 15);
  //central nodes by both metric one & metri two
  img.draw_circle(loc3, loc2, legendNodeSize, mixed);
  img.draw_text(loc4, loc2, "Centre with both metrics", white, 0, 1, 15);

  img.save("genvisOutput.bmp");
}

int main(int argc, char** argv) {

   FILE* inputFile;
   FILE* centreFile;
   FILE* coordFile;
   struct node* input;
   struct centreNodes* centres;
   int** coords;

   //checking inputted arguments correct
   if (argc == 4){
      inputFile = fopen(argv[1], "r");
      centreFile = fopen(argv[2], "r");
      coordFile = fopen(argv[3], "r");

      if (inputFile == NULL){
         printf("First inputted file not found.\n");
         return 1;
      } else if (centreFile == NULL){
	printf("Second inputted file not found.\n");
	return 1;
      } else if (coordFile == NULL){
        printf("Third inputted file not found.\n");
	return 1;
      }

   } else {
      printf("Need three files as input.\n");
      return 1;
   }

  //inputted nodes as struct node*
  input = read_nodeFile(inputFile);

  int totalNodes = input[0].totalNodes;
  //the centre nodes
  centres = read_centreOutput(centreFile);

  //coordinates
  coords = read_CoordsFile(coordFile, totalNodes);

  //drawing the graph
  drawGraph(input, centres, coords, totalNodes);

  //freeing malloc'd memory
  freeNodesPointer(input);
  freeCentreNodesPointer(centres);
  freeCoords(coords, totalNodes);
  return 0;
}
